package by.vitalylobatsevich.tictactoe.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.http.HttpMethod;

@Configuration
public class SecurityConfiguration {

    @Bean
    public SecurityFilterChain filterChain(final HttpSecurity http) throws Exception {
        return http
            .authorizeHttpRequests(
                customizer -> customizer
                    .antMatchers(
                        HttpMethod.GET,
                        "/",
                        "/css/**",
                        "/js/**"
                    ).permitAll()
                    .anyRequest().authenticated()
            )
            .build();
    }

}
